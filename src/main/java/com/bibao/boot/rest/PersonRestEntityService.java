package com.bibao.boot.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bibao.boot.dao.PersonDao;
import com.bibao.boot.model.Person;

@RestController
@RequestMapping("/entity")
public class PersonRestEntityService {
	@Autowired
	private PersonDao personDao;
	
	@GetMapping("/{id}")
	public ResponseEntity<Person> getPersonById(@PathVariable("id") int id) {
		Person person = personDao.findById(id);
		if (person==null) {
			return new ResponseEntity<Person>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Person>(person, HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<List<Person>> getAllPerson() {
		List<Person> personList = personDao.findAll();
		if (CollectionUtils.isEmpty(personList)) {
			return new ResponseEntity<List<Person>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Person>>(personList, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Person> savePerson(@RequestBody Person person) {
		Person savedPerson = personDao.save(person);
		return new ResponseEntity<Person>(savedPerson, HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Person> updatePerson(@RequestBody Person person) {
		Person updatedPerson = personDao.update(person);
		return new ResponseEntity<Person>(updatedPerson, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public void deletePersonById(@PathVariable("id") int id) {
		personDao.deleteById(id);
	}
}
