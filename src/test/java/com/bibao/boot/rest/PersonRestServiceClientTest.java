package com.bibao.boot.rest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.bibao.boot.model.Person;

public class PersonRestServiceClientTest {
	private static final String BASE_URI = "http://localhost:8080/spring-boot-rest/rest/person";
	private static final String ID_URI = BASE_URI + "/{id}";
	private RestTemplate template;
	
	@Before
	public void setUp() throws Exception {
		template = new RestTemplate();
	}

	@Test
	public void testFindPersonById() {
		Person person = template.getForObject(ID_URI, Person.class, "1");
		assertEquals(1, person.getId());
		assertEquals("Alice", person.getFirstName());
		assertEquals("Liu", person.getLastName());
	}

	@Test
	public void testFindAllPerson() {
		@SuppressWarnings("unchecked")
		List<Person> personList = template.getForObject(BASE_URI, List.class);
		assertFalse(CollectionUtils.isEmpty(personList));
	}
	
	@Test
	public void testSavePerson() {
		Person person = new Person();
		person.setFirstName("Alex");
		person.setLastName("Wang");
		Person savedPerson = template.postForObject(BASE_URI, person, Person.class);
		assertEquals(4, savedPerson.getId());
		assertEquals("Alex", savedPerson.getFirstName());
		assertEquals("Wang", savedPerson.getLastName());
	}
}
